import { RpcDefinition } from "@litbase/core";

export const customAction = new RpcDefinition<[], void>("customAction");
