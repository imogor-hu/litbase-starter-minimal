import { registry, RpcRequest } from "@litbase/core";
import { customAction } from "@my-app/common/actions/actions";

export function registerActions() {
  registry.register(customAction, custom);
}

async function custom(request: RpcRequest) {
  console.debug("Action running on the backend", request);
}
