import { build } from "esbuild";
import fs from "fs";

const pkg = JSON.parse(fs.readFileSync("./package.json"));

await build({
  external: [...Object.keys(pkg.dependencies), ...Object.keys(pkg.devDependencies)],
  entryPoints: ["./src/index.ts"],
  bundle: true,
  platform: "node",
  format: "esm",
  target: "node16",
  outdir: "dist",
  loader: {
    ".png": "file",
    ".jpg": "file",
  },
});
