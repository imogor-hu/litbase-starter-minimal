import { ThemeProvider } from "styled-components";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ModalProvider } from "@litbase/alexandria";
import { TopScroller } from "@litbase/alexandria/components/utils/top-scroller";
import { GlobalStyle, theme } from "./styles/theme-types";

export function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <BrowserRouter>
        <TopScroller />
        <ModalProvider>
          <Routes>
            <Route index element={<div>Hello, there</div>}></Route>
          </Routes>
        </ModalProvider>
      </BrowserRouter>
    </ThemeProvider>
  );
}
