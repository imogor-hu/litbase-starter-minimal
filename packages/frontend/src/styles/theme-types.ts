import { baseGlobalStyle, createTheme } from "@litbase/alexandria";
import { createGlobalStyle } from "styled-components";

export const theme = createTheme({});

export const GlobalStyle = createGlobalStyle`
  ${baseGlobalStyle};
`;
