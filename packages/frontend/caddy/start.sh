#!/usr/bin/env sh

# Replace occurrences of these strings with the value of the env variable
find /srv/* -type f -exec sed -i -- "s~process.env.SERVER_BASE_URL~\"$SERVER_BASE_URL\"~g" {} \;
caddy run --config /etc/caddy/Caddyfile --adapter caddyfile
