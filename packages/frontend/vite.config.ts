import { defineConfig } from "vite";
import reactSvgPlugin from "vite-plugin-svgr";
import react from "@vitejs/plugin-react";
import path from "path";
import dotenvFlow from "dotenv-flow";
import { ViteEjsPlugin } from "vite-plugin-ejs";

dotenvFlow.config({ path: "../../" });

const defines = Object.fromEntries(Object.entries(process.env).map(([key, value]) => [`process.env.${key}`, JSON.stringify(value)]));

// https://vitejs.dev/config/
export default defineConfig(({ command }) => ({
  plugins: [
    react({
      babel: {
        plugins: [
          [
            "babel-plugin-styled-components",
            command === "serve"
              ? {
                  displayName: true,
                  minify: false,
                  transpileTemplateLiterals: false,
                }
              : {
                  displayName: false,
                  pure: true,
                  minify: true,
                  transpileTemplateLiterals: true,
                },
          ],
        ],
      },
    }),
    reactSvgPlugin({
      exportAsDefault: true,
      svgrOptions: {
        plugins: ["@svgr/plugin-svgo", "@svgr/plugin-jsx"],
      },
    }),
    ViteEjsPlugin({
      command,
      ...defines,
    }),
  ],
  optimizeDeps: {
    exclude: ["assets/js"],
  },
  server: {
    port: 3000
  },
  resolve: {
    alias: {
      "@my-app/common": path.resolve(__dirname, "../common/src/"),
    },
  },
  define: {
    ...defines,
  },
}));
